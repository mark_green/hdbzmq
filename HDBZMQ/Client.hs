module HDBZMQ.Client (
    runClient
) where

import HDBZMQ.Client.API

runClient = do
    connect
    print "Client!"