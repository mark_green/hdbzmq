module HDBZMQ.Client.API (
    connect,
    DocumentKey,
    DocumentBody,
    Document(..),
    TransactionId,
    Transaction(..),
    Response(..),
    Msg(..)
) where

type DocumentKey = String
type DocumentBody = String
data Document = Document DocumentKey DocumentBody
        deriving (Show)

type TransactionId = String
data Transaction = PutTransaction TransactionId Document
        | GetTransaction TransactionId DocumentKey
        | CompositeTransaction TransactionId [Transaction]
        | PrintTransaction
        deriving (Show)
data Response = GetResponse TransactionId DocumentBody
        | NotFoundResponse TransactionId
        deriving (Show)

data Msg = TransactionMsg Transaction
         | ResponseMsg Response
         deriving (Show)

connect = do
    print "Connected!"