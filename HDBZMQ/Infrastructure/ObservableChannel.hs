module HDBZMQ.Infrastructure.ObservableChannel (
    ObservableChannel,
    newObservableChannel,
    readObservableChannel,
    writeObservableChannel,
    lengthOfObservableChannel,
    getSmallestObservableChannel
) where

import           Control.Concurrent (Chan, newChan, readChan, writeChan)
import           Control.Concurrent.STM.TMVar (TMVar)
import qualified Control.Concurrent.STM.TMVar as TMVar
import           Control.Monad (mapM)
import           Control.Monad.STM (atomically)
import           Data.List (sortBy)
import           Data.Ord (comparing)

data ObservableChannel a = ObservableChannel (Chan a) (TMVar Int)

newObservableChannel :: IO (ObservableChannel a)
newObservableChannel = do
    channel <- newChan
    channelLength <- atomically $ TMVar.newTMVar 0
    return $ ObservableChannel channel channelLength

readObservableChannel :: ObservableChannel a -> IO a
readObservableChannel (ObservableChannel channel channelLength) = do
    result <- readChan channel
    atomically $ do
        len <- TMVar.takeTMVar channelLength
        TMVar.putTMVar channelLength (len-1)
    return result

writeObservableChannel :: ObservableChannel a -> a -> IO ()
writeObservableChannel (ObservableChannel channel channelLength) x = do
    writeChan channel x
    atomically $ do
        len <- TMVar.takeTMVar channelLength
        TMVar.putTMVar channelLength (len+1)

lengthOfObservableChannel :: ObservableChannel a -> IO Int
lengthOfObservableChannel (ObservableChannel _ channelLength) =
    atomically $ TMVar.readTMVar channelLength

getSmallestObservableChannel :: [ObservableChannel a] -> IO (ObservableChannel a)
getSmallestObservableChannel channels = do
    channelsWithLengths <- atomically $ mapM
        (\channel@(ObservableChannel _ channelLength) -> do
            channelLength <- TMVar.readTMVar channelLength
            return (channel, channelLength)) channels
    return $ fst.head $ sortBy (comparing snd) channelsWithLengths