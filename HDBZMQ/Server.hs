module HDBZMQ.Server (
    runServer
) where

import           Control.Monad (forever, forM, mapM, replicateM)
import qualified Control.Monad.State as State
import           Data.Binary
import           Data.Map (Map)
import qualified Data.Map as Map

import HDBZMQ.Client.API
import HDBZMQ.Infrastructure

runServer nextMessageFromTransport bus = do
    transactor <- queuedPersistentStatefulHandler "db.dat" transactionHandler Map.empty
    forever $ do
        message <- nextMessageFromTransport
        routeMessageTo transactor bus message

transactionHandler :: StatefulMessageHandler Msg (Map DocumentKey DocumentBody)
transactionHandler = StatefulMessageHandler handle where
    handle bus@(MessageHandler publish) (TransactionMsg transaction)  = do
        case transaction of
            PutTransaction transactionId (Document key body) -> do
                State.modify $ Map.insert key body
            GetTransaction transactionId key -> do
                docs <- State.get
                case Map.lookup key docs of
                    Just body -> State.lift $ publish bus $ ResponseMsg $ GetResponse transactionId body
                    otherwise -> State.lift $ publish bus $ ResponseMsg $ NotFoundResponse transactionId
            CompositeTransaction transactionId transactions -> State.lift $ do
                forM transactions (\transaction' ->
                    publish bus $ TransactionMsg transaction')
                return ()
            PrintTransaction -> do
                docs <- State.get
                State.lift $ print docs
        return ()
    handle bus@(MessageHandler publish) _  = return ()