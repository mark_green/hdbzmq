module HDBZMQ.Infrastructure (
    MessageHandler(..),
    emptyHandler,
    loadBalancedHandler,
    loggingHandler,
    multiplexingHandler,
    printingHandler,
    queuedHandler,
    routeMessageTo,
    sendAfterDelay,

    StatefulMessageHandler(..),
    queuedPersistentStatefulHandler,
    queuedStatefulHandler,
    roundRobinHandler,

    ProcessManagerRunnerSpec(..),
    ProcessManagerMessageType(..),
    processManagerRunnerHandler
) where

import           Control.Concurrent (forkIO, newChan, readChan, writeChan, threadDelay)
import           Control.Concurrent.STM.TMVar (TMVar)
import qualified Control.Concurrent.STM.TMVar as TMVar
import           Control.Monad (forever, forM)
import qualified Control.Monad.State as State
import           Control.Monad.STM (STM, atomically)
import           Data.Binary
import           Data.Map (Map)
import qualified Data.Map as Map
import           Data.Time.Clock (getCurrentTime, utctDayTime)
import           System.Directory (doesFileExist)

import           HDBZMQ.Infrastructure.ObservableChannel

-- | MessageHandler
--   A wrapper for functions which react to Messages
data MessageHandler m = MessageHandler (MessageHandler m -> m -> IO ())

emptyHandler :: MessageHandler m
emptyHandler = MessageHandler (\_ _ -> return ())

loadBalancedHandler :: [MessageHandler m] -> IO (MessageHandler m)
loadBalancedHandler handlers = do
    channels <- forM handlers
        (\(MessageHandler handle) -> do
            channel <- newObservableChannel
            forkIO . forever $ do
                (bus, message) <- readObservableChannel channel
                handle bus message
            return channel)
    return $ MessageHandler $ handle channels where
        handle channels bus message = do
            channel <- getSmallestObservableChannel channels
            writeObservableChannel channel (bus, message)

loggingHandler :: (Show m) => MessageHandler m
loggingHandler = MessageHandler handle where
    handle _ message = do
        utcNow <- getCurrentTime
        print $ (show (utctDayTime utcNow))++": "++(show message)

multiplexingHandler :: [MessageHandler m] -> MessageHandler m
multiplexingHandler handlers = MessageHandler handle where
    handle bus message = do
        forM handlers (\handler -> routeMessageTo handler bus message)
        return ()

printingHandler :: (Show m) => MessageHandler m
printingHandler = MessageHandler handle where
    handle _ message = print message

queuedHandler :: MessageHandler m -> IO (MessageHandler m)
queuedHandler (MessageHandler handle) = do
    channel <- newChan
    forkIO . forever $ do
        (bus, message) <- readChan channel
        handle bus message
    return $ MessageHandler $ handle' channel where
        handle' channel bus message = writeChan channel (bus,message)

routeMessageTo :: MessageHandler m -> MessageHandler m -> m -> IO ()
routeMessageTo (MessageHandler handle) bus message =
    handle bus message

sendAfterDelay :: Int -> MessageHandler m -> m -> IO ()
sendAfterDelay delayInMicroseconds bus message = do
    forkIO $ do
        threadDelay delayInMicroseconds
        routeMessageTo bus bus message
    return ()


-- | StatefulMessageHandler
--   A wrapper for handlers which keep some state
data StatefulMessageHandler m s = StatefulMessageHandler (MessageHandler m -> m -> State.StateT s IO ())

queuedPersistentStatefulHandler :: (Binary s) => String -> StatefulMessageHandler m s -> s -> IO (MessageHandler m)
queuedPersistentStatefulHandler file (StatefulMessageHandler handle) initialState = do
    fileExists <- doesFileExist file
    state <- if fileExists then
                decodeFile file
             else
                return initialState
    channel <- newChan
    let runStatefulHandler = (\state -> do
                (bus, message) <- readChan channel
                state' <- State.execStateT (handle bus message) state
                encodeFile file state'
                runStatefulHandler state')
    forkIO $ runStatefulHandler state
    return $ MessageHandler (\bus message -> writeChan channel (bus,message))

queuedStatefulHandler :: StatefulMessageHandler m s -> s -> IO (MessageHandler m)
queuedStatefulHandler (StatefulMessageHandler handle) initialState = do
    channel <- newChan
    let runStatefulHandler = (\state -> do
                (bus, message) <- readChan channel
                state' <- State.execStateT (handle bus message) state
                runStatefulHandler state')
    forkIO $ runStatefulHandler initialState
    return $ MessageHandler (\bus message -> writeChan channel (bus,message))

roundRobinHandler :: StatefulMessageHandler m [MessageHandler m]
roundRobinHandler = StatefulMessageHandler handle where
    handle bus message = do
        handlers <- State.get
        let handler = head handlers
        State.lift $ routeMessageTo handler bus message
        State.put $ (tail handlers)++[handler]

-- | ProcessManagerRunnerSpec
--   A wrapper for a process manager runner, routes messages to process managers with lifetimes defined by message types
data ProcessManagerRunnerSpec message correlationId =
    ProcessManagerRunnerSpec (message -> ProcessManagerMessageType) (message -> correlationId) (message -> IO (MessageHandler message))
data ProcessManagerMessageType = StartProcessManager
                               | HandleNormally
                               | EndProcessManager

processManagerRunnerHandler :: (Ord c) => ProcessManagerRunnerSpec m c -> StatefulMessageHandler m (Map c (MessageHandler m))
processManagerRunnerHandler (ProcessManagerRunnerSpec getMessageType getCorrelationId getProcessManager) = StatefulMessageHandler handle where
    handle bus@(MessageHandler publish) message = do
        case getMessageType message of
            StartProcessManager -> do
                processManager <- State.lift $ getProcessManager message
                State.modify $ Map.insert (getCorrelationId message) processManager
            EndProcessManager   -> State.modify (Map.delete (getCorrelationId message))
            HandleNormally      -> return ()
        processManagers <- State.get
        if Map.member (getCorrelationId message) processManagers then
            State.lift $ routeMessageTo (processManagers Map.! (getCorrelationId message)) bus message
        else
            return ()