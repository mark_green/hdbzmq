module Main where

import Control.Concurrent (threadDelay)
import Data.Char (toLower)

import HDBZMQ.Client.API
import HDBZMQ.Infrastructure
import HDBZMQ.Server

readFromTransport = do
	putStrLn "G for get, P for Put> "
	cmd <- getChar
	case toLower cmd of
		'g' -> return (TransactionMsg $ GetTransaction "001" $ "jimbo/whatever")
		'p' -> return (TransactionMsg $ PutTransaction "002" $ Document "jimbo/whatever" "{howzit: 'going?'}")
		otherwise -> do
			putStrLn "No such command"
			readFromTransport

main = runServer readFromTransport loggingHandler